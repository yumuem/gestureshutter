var ShutterView = (function(){


  function ShutterView(id){
    this.canvas = document.getElementById(id);
    this.ctx = this.canvas.getContext('2d');
  }


  ShutterView.prototype.play = function(x,y){
      this.drawArc(x,y, 5, '#ff0000');
      console.log("x:"+x+",y:"+y);
  }

  ShutterView.prototype.drawArc = function(x, y, r, c){
    this.ctx.save();
    this.ctx.fillStyle = c;
    this.ctx.beginPath();
    this.ctx.arc(x, y, r, 0, 2 * Math.PI);
    this.ctx.fill();
    this.ctx.restore();
  }

  return ShutterView;
})();
