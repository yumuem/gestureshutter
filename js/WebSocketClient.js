var WebSocketClient = (function(){
    var webSocket_ = null;

    function WebSocketClient(){
    }
//    var dump = function(obj) {
    WebSocketClient.prototype.dump = function(obj) {
      var properties = new Array();
      for (var prop in obj){
        properties.push(prop + '=' + obj[prop] + '\n');
      }
      console.log(properties.join(''));
    }

    function showPostView(t) {
      var canvas= document.getElementById('postviewcanvas');
      var ctx= canvas.getContext('2d');
      var postimage = new Image();
      postimage.src = t;
      ///postimage.src = "pict20131104_161537_0.jpg";
      postimage.onload = function(){
        ctx.drawImage(postimage,0,0);
      };
    }

//    var wsOpen = function() {
    WebSocketClient.prototype.wsOpen = function() {
      var wsc = this;
      if (webSocket_) {
        return;
      }
      webSocket_ = new WebSocket('ws://localhost:8025/websockets/echo');
      webSocket_.onopen = function(e) {
        wsc.dump(e);
      }
      webSocket_.onclose = function(e) {
        wsc.dump(e);
        webSocket_ = null;
      }
      webSocket_.onmessage = function(e) {
        wsc.dump(e);
        //var msg = document.getElementById('msg');
        //msg.innerHTML = e.data + '<br/>' + msg.innerHTML;
        //msg.value = e;
        if( e.data.substr(-4) ==".JPG"){
            showPostView(e.data);
        }
      }
      webSocket_.onerror = function(e) {
        wsc.dump(e);
        webSocket_ = null;
      }
    }

//    var wsClose = function() {
    WebSocketClient.prototype.wsClose = function() {
      if (webSocket_) {
        webSocket_.close(1000, 'Closed by user.');
        webSocket_ = null;
      }
    }

//    var wsSend = function(t) {
    WebSocketClient.prototype.wsSend = function(t) {
      if (webSocket_) {
//        console.log('send message : ' + t.value);
//        webSocket_.send(t.value);
        console.log('send message : ' + t);
        webSocket_.send(t);
      }
    }


/*
    WebSocketClient.prototype.showPostView = function(t) {
      var canvas= document.getElementById('postviewcanvas');
      var ctx= canvas.getContext('2d');
      var postimage = new Image();
      postimage.src = t;
      ///postimage.src = "pict20131104_161537_0.jpg";
      postimage.onload = function(){
        ctx.drawImage(postimage,0,0);
      };
    }
*/

    return WebSocketClient;
})();

