var MainActivity = (function(){
  function MainActivity(){
    root = this;
    this.delay = null;

    window.onload = function () {
      root.ws = new WebSocketClient();

      Init();
      root.startMotionCapture();
    }
  }

  function Init(){
    // fullscreen
        var canvas= document.getElementById('bgcanvas');
 //       canvas.width = document.body.clientWidth;
 //       canvas.height = document.body.clientHeight;
        var ctx= canvas.getContext('2d');
        var image = new Image();

        image.src = "camera.png";
        image.onload = function(){
          ctx.drawImage(image, canvas.width/2 - image.width/2, canvas.height/2 - image.height/2, image.width, image.height);
        }

        root.ws.wsOpen();
  
        var btn = document.getElementById('SearchDeviceButton');
        btn.addEventListener('click', startsearchDevice, false);
  //      var btn = document.getElementById('PostViewButton');
  //      btn.addEventListener('click', showPostView, false);
  }

  function startsearchDevice() {
      root.ws.wsSend("Search");
  }


  function TakePicture() {
      root.ws.wsSend("TakePicture");
  }


  MainActivity.prototype.startMotionCapture = function(){
    canvas= document.getElementById('shuttercanvas');
//    canvas.width = document.body.clientWidth;
//    canvas.height = document.body.clientHeight;
    ctx2= canvas.getContext('2d');
    ctx2.translate(canvas.width/2,canvas.height);
      
    Leap.loop({enableGestures: true}, function(frame){
      ctx2.fillStyle = "rgba(0,0,0,0.7)";
      // clear last frame
      ctx2.clearRect(-canvas.width/2,-canvas.height,canvas.width,canvas.height);

      // Gesture point and show string
      var gestureString = "";
      if (frame.gestures.length > 0) {
        for (var i = 0; i < frame.gestures.length; i++) {
          var gesture = frame.gestures[i];
          switch (gesture.type) {
            case "screenTap":
            case "keyTap":
              var g_pos = gesture.position;
              var g_direction = gesture.direction;
//              gestureString = " Tap:("+g_pos[0]+","+g_pos[1]+","+g_pos[2]+")";
              gestureString = " Tap:("+g_pos[0]+","+g_pos[1]+")";
              TakePicture();
              break;
            case "circle":
              gestureString = "Circle";
              break;
            case "swipe":            
              gestureString = "Swipe";
              break;
            default:
              gestureString += "unkown gesture type";
              break;
          }
        }
        document.getElementById('gesture').innerHTML = "<div><p> Gesture:"+gestureString+"</p></div>";
      }

      // render circles based on pointable positions
      var pointablesMap = frame.pointablesMap;
      for (var i in pointablesMap) {
        // get the pointable's position
       //var pointable = pointablesMap[0];
        var pointable = pointablesMap[i];
        var pos = pointable.tipPosition;

        // create a circle for each pointable
        var radius = Math.min(600/Math.abs(pos[2]),20);
        ctx2.beginPath();
        var x = pos[0]-radius/2;
        var y = -pos[1]-radius/2;
        ctx2.arc(pos[0]-radius/2,-pos[1]-radius/2,radius,0,2*Math.PI);
        ctx2.fill();
        console.log("x:"+x+",y:"+y );
      }
      
    });
    
  }
  return MainActivity;
})();


new MainActivity();



